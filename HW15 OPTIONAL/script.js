function fact(n) {
  if (n === 1) {
    return 1;
  }

  return fact(n - 1) * n;
}

let result = prompt('Enter factorial number')

console.log(fact(result))
