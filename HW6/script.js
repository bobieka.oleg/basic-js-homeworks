function filterBy(arr, dataType) {

  return arr.filter(item => {
    return typeof item !== dataType;
  })
}

console.log(filterBy([undefined, 3, 'Dog', Symbol(), {}, Boolean, null], 'symbol'));
