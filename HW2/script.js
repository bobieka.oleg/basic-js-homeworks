// First variant
// let multiple = +prompt('Enter some number bigger than 0! BUT This number must be an integer')
//
// for (let i = 1; i <= multiple; i++) {
//   if (i % 5 == 0) {
//     console.log(i)
//   }
//   else if (multiple <= 4) {
//     console.log('Sorry, no number')
//   }
// }

// Second variant

let multiple;
do {
  multiple = +prompt('Enter some number bigger than 0! BUT This number must be an integer')
} while (!Number.isInteger(multiple))

if (multiple < 5) {
  console.log('Sorry no numbers')
} else {
  for (let i = 1; i <= multiple; i++) {
    if (i % 5 == 0) {
      console.log(i)
    }
  }
}
