function createNewUser(name, lastname) {
  let birthday = prompt('Enter your birthday please - like "dd.mm.yyyy"')

  let firstName = name;
  let lastName = lastname;

  let newUser = {
    birthday,
    firstName,
    lastName,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase()
    },
    getAge() {
      return Math.floor(
        (new Date() -
          new Date(
            this.birthday.slice(3, 5) +
            "," +
            this.birthday.slice(0, 2) +
            "," +
            this.birthday.slice(6, 10)
          )) /
        (1000 * 60 * 60 * 24 * 365)
      );
    },
    getPassword() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10)
    }
  }

  Object.defineProperty(newUser,
    'firstName', { configurable: false
    })
  Object.defineProperty(newUser,
    'lastName', { configurable: false
    })
  return newUser;
}

let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'))

console.log(newUser.getAge())
console.log(newUser.getPassword())
