function createNewUser(name, lastname) {

  let firstName = name;
  let lastName = lastname;

  let newUser = {
    firstName,
    lastName,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase()
    }
  }
  Object.defineProperty(newUser,
    'firstName', { configurable: false
  })
  Object.defineProperty(newUser,
    'lastName', { configurable: false
  })
  return newUser;
}

let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'))

console.log(newUser.getLogin())
